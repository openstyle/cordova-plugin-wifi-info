
var exec = require('cordova/exec');

var PLUGIN_NAME = 'WifiInfo';

var wifiInfo = {
  currentSSID: function(success, error) {
    exec(success, error, PLUGIN_NAME, 'currentSSID', []);
  }
};

module.exports = wifiInfo;
