/**
 */
package it.saep.cordova.wifi_info;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.util.Log;

public class WifiInfo extends CordovaPlugin
{
    private static final String TAG = "WifiInfo";

    private static final String ACTION_CURRENTSSID = "currentSSID";

    @SuppressWarnings("serial")
    class WifiInfoException extends Exception
    {
		public WifiInfoException(String message) { super(message); }
    }

    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);

        Log.d(TAG, "Initializing WifiInfo plugin");
    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException
    {
        try
        {
            if (ACTION_CURRENTSSID.equals(action))
            {
                String currentSsid = getCurrentSSID();
                PluginResult result = new PluginResult(PluginResult.Status.OK, currentSsid);
                callbackContext.sendPluginResult(result);
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            callbackContext.error(e.getMessage());
            return true;
        }
    }

    private String getCurrentSSID() throws WifiInfoException
    {
        WifiManager wifiManager = (WifiManager) cordova.getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled())
        {
            Log.w(TAG, "Wifi is not enabled");
            throw new WifiInfoException("Wifi is not enabled");
        }

        android.net.wifi.WifiInfo info = wifiManager.getConnectionInfo();
        if (info == null)
        {
            Log.w(TAG, "WifiManager.getConnectionInfo() returned nothing");
            throw new WifiInfoException("WifiManager.getConnectionInfo() returned nothing");
        } 

        if (info.getSupplicantState() != SupplicantState.COMPLETED)
        {
            Log.w(TAG, "SupplicantState != COMPLETED");
            throw new WifiInfoException("Wifi is not connected");
        }
        
        // se non abbiamo i permessi vengono ritornati dei valori specifici
        if ("02:00:00:00:00:00".equals(info.getBSSID()))
        {
            Log.w(TAG, "Invalid BSSID, probably missing the appropriate permissions");
            throw new WifiInfoException("Invalid BSSID, check that all required permissions have been granted to the app");
        }

        String ssid = info.getSSID();
        
        // supportiamo solo i SSID "printable", che Android ritorna tra ""
        if (!ssid.startsWith("\""))
        {
            Log.e(TAG, "We only support printable SSIDs");
            throw new WifiInfoException("SSID cannot be converted to an UTF-8 string");
        }

        ssid = ssid.substring(1, ssid.length() - 1);

        Log.i(TAG, "SSID is \"" + ssid + "\"");

        return ssid;
    }

}
