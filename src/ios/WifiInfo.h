#import <Cordova/CDVPlugin.h>

@interface WifiInfo : CDVPlugin {
}

- (void)currentSSID:(CDVInvokedUrlCommand *)command;

@end
