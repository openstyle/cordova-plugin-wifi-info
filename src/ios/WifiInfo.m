#import "WifiInfo.h"

#import <SystemConfiguration/CaptiveNetwork.h>

@implementation WifiInfo

- (void)pluginInitialize {
}

- (void)currentSSID:(CDVInvokedUrlCommand *)command {
  
  NSString *ssid = currentWifiSSID();

  CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:ssid];

  [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

// https://stackoverflow.com/questions/5198716/iphone-get-ssid-without-private-library/14288648#14288648
static NSString *currentWifiSSID() {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid;
}

@end
